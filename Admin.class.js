class Admin extends User {

  constructor (firstName, username, permissionLevel) {
    super(firstName, username, Date.now())
    this.permissionLevel = permissionLevel
  }

  cancelSubscription (user) {
    // ...
  }

  login (password) {
    // ...
  }

}
