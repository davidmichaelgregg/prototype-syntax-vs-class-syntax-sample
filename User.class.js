class User {
  constructor (firstName, username, creationDate) {
    this.firstName = firstName
    this.username = username
    this.creationDate = creationDate
  }

  login (password) {
    // ...
  }

  logout () {
    // ...
  }
}

new User('Bill', 'megatronTruckGuy', 45678965)