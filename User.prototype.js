function User (firstName, username, creationDate) {
  this.firstName = firstName
  this.username = username
  this.creationDate = creationDate
}

User.prototype = {
  constructor: User,

  login: function (password) {
    // ...
  },

  logout () {
    // ...
  }
}

new User('Bill', 'megatronTruckGuy', 45678965)