function Admin (firstName, username, permissionLevel, creationDate) {
  User.call(this, firstName, username, creationDate)
  this.permissionLevel = permissionLevel
}

Admin.prototype = Object.create(User.prototype)
Admin.prototype.constructor = Admin

Admin.prototype.cancelSubscription = function (user) {
  // ...
}

Admin.prototype.login = function (password) {
  // ...
}

const randy = new Admin()
randy.login('pink bubblegum refridgerator')
